#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split, StratifiedKFold, GridSearchCV, cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, roc_auc_score, roc_curve
from sklearn.metrics import classification_report,precision_score, recall_score
from sklearn.decomposition import PCA


# In[2]:


#Import Data
for direname,_,filename in os.walk("data"):
    for filename in filenames:
        print(os.path.join(direname,filename))


# In[5]:


os.getcwd()


# In[16]:


os.chdir("/Users/fangchen/Desktop/UC/11523Data science tech&system/Week4")


# In[19]:


#load data
data_path="/Users/fangchen/Desktop/UC/11523Data science tech&system/Week4/weatherAUS.csv"
df=pd.read_csv(data_path)
df.head()


# In[21]:


#data shape
print(df.shape)
#column names
print(df.columns)
#data summary


# In[ ]:


print(df.info())
#type of variables
#categorical
catagorical=[var for var in df.columns if df[var].dtype=="object"]
print("There are () categorical variables\n".format(len(categorical)))
print("The categorical variables are:",categorical)


# In[23]:


#check if target variable has any null/na value
df.isnull().sum()


# # Explore categorical variables

# In[ ]:


#check missing value
print(df[categorical].isnull().sum())
print("******\nPercentage of missing value\n******")
print(100*df[categorical].isnull().sum()/df.shape[0])


# In[ ]:


#check which variables has missing values
cat_with_missing=[var for var in categorical if df[var].isnull().sum()>0]
print(df[cat_with_missing].isnull().sum())


# In[ ]:


#remove missing values from numerical features
numerical = df._get_numeric_data().columns
categorical = set(df.columns) - set(numerical)
loc_for_miss = df["Location"].unique().tolist()
ls = []
def removeNull(columns):
    j=0
    while j<=len(columns)-1:
        for i in range(len(loc_for_miss)):    
            ls.append(str(df.loc[df["Location"] == loc_for_miss[i], columns[j]].median())) #great outliers so used median

        for i in range(len(loc_for_miss)):
            df.loc[df["Location"] == loc_for_miss[i], columns[j]] = df.loc[df["Location"] == loc_for_miss[i], columns[j]].fillna(ls[i])
        j+=1
        df[columns] = df[columns].astype(float)
        
removeNull(numerical)


# In[ ]:


def plotHist():
    fig, ax =plt.subplots(5,3, figsize=(15,15))
    i=0;j=0;k=0
    while i<=4:
        while j<=2:
            sns.distplot(df[numerical[k]], ax=ax[i, j])
            j+=1;k+=1
        j=0;i+=1; 
    plt.savefig('distribution_before_removing_missing_values_from_numerical_columns.png')
    plt.show()
plotHist()


# In[ ]:


#check outliers in numerical columns
num_of_rows = 4
num_of_cols = 4
fig, ax = plt.subplots(num_of_rows, num_of_cols, figsize=(15,15))
print(numerical)
i=0;j=0;k=0;
while i<num_of_rows:
    while j<num_of_cols:
        sns.boxplot(df[numerical[k]], ax=ax[i, j])
        k+=1;j+=1
    j=0;i+=1
plt.savefig('before_removing_outliers_from_numerical_columns.png')
plt.show()


# In[ ]:


#remove outliers
lsUpper = []
lsLower = []
def removeOutliers(numerical):
    for i in range(len(numerical)):
        q1 = df[numerical[i]].quantile(0.25)
        q3 = df[numerical[i]].quantile(0.75)
        IQR = q3-q1
        minimum = q1 - 1.5 * IQR
        maximum = q3 + 1.5 * IQR
        df.loc[(df[numerical[i]] <= minimum), numerical[i]] = minimum
        df.loc[(df[numerical[i]] >= maximum), numerical[i]] = maximum  
removeOutliers(numerical)


# In[ ]:


num_of_rows = 4
num_of_cols = 4
fig, ax = plt.subplots(num_of_rows, num_of_cols, figsize=(15,15))
print(numerical)
i=0;j=0;k=0;
while i<num_of_rows:
    while j<num_of_cols:
        sns.boxplot(df[numerical[k]], ax=ax[i, j])
        k+=1;j+=1
    j=0;i+=1
plt.savefig('after_removing_outliers_from_numerical_columns.png')
plt.show()


# In[ ]:


def plotHist():
    fig, ax =plt.subplots(5,3, figsize=(15,15))
    i=0;j=0;k=0
    while i<=4:
        while j<=2:
            sns.distplot(df[numerical[k]], ax=ax[i, j])
            j+=1;k+=1
        j=0;i+=1;
    plt.savefig('distribution_after_removing_outliers_from_numerical_columns.png')
    plt.show()
plotHist()


# In[ ]:


####remove missing values from categorical variables
# For WindGustDir column only
ls_WGD = []
loc_for_miss = df["Location"].unique()
for j in range(len(loc_for_miss)):
    df_allNanWGD = df.loc[df["Location"] == loc_for_miss[j]]
    if(df_allNanWGD["WindGustDir"].isnull().all()):
        ls_WGD.append(loc_for_miss[j])


# In[ ]:


#first handling these cities only
# 'Newcastle' is near to "Sydnay" with mode W,
# 'Albany' is near to "Perth" with mode SW

# Imputing W in Newcastle for WindGustDir column
df.loc[df["Location"] == "Newcastle", "WindGustDir"] = "W"
df.loc[df["Location"] == "Albany", "WindGustDir"] = "SW"


# In[ ]:


#mode in categorical varibles wrt cities
numerical = df._get_numeric_data().columns
# print("numerical features are", numerical)
# categorical = list(set(df.columns) - set(numerical))
categorical = ['RainToday', 'WindDir9am', 'WindDir3pm']
loc_for_miss = ['Albury','BadgerysCreek', 'Cobar', 'CoffsHarbour', 'Moree','Newcastle','NorahHead','NorfolkIsland',
                'Penrith','Richmond','Sydney','SydneyAirport','WaggaWagga','Williamtown','Wollongong','Canberra',
                'Tuggeranong','MountGinini','Ballarat','Bendigo','Sale','MelbourneAirport','Melbourne','Mildura',
                'Nhil','Portland','Watsonia','Dartmoor','Brisbane','Cairns','GoldCoast','Townsville','Adelaide',
                'MountGambier','Nuriootpa','Woomera','Albany','Witchcliffe','PearceRAAF','PerthAirport','Perth',
                'SalmonGums','Walpole','Hobart','Launceston','AliceSprings','Darwin','Katherine','Uluru']
ls = []
ls_allNAN = []
def removeNull(columns):
    for j in range(len(columns)):
        ls = []
        for i in range(len(loc_for_miss)): 
            ls.append(df.loc[df["Location"] == loc_for_miss[i], columns[j]].mode()[0])
        for i in range(len(loc_for_miss)):
            df.loc[df["Location"] == loc_for_miss[i], columns[j]] = df.loc[df["Location"] == loc_for_miss[i], columns[j]].fillna(ls[i])
        df[columns] = df[columns].astype(object)
    
removeNull(categorical)


# In[ ]:


categoricalPlot = ['RainToday', 'WindDir9am','WindGustDir', 'WindDir3pm']
fig, ax = plt.subplots(4, 1, figsize=(15,15))
print(categoricalPlot)
c=0
while c<=3:
    sns.countplot(df[categoricalPlot[c]], ax=ax[c])
    plt.xticks(rotation=90)
    c+=1
plt.savefig('distribution_after_removing_missing_values_from_categorical_columns.png')
plt.show()


# # Feature engineering

# In[ ]:


#Adding extra columns by splitting date column
df['Year']=[d.split('-')[0] for d in df.Date]
df['Year'].astype(float)
df['Month']=[d.split('-')[1] for d in df.Date]
df['Day']=[d.split('-')[2] for d in df.Date]
df.drop(columns=["Date"], axis=1, inplace=True)


# In[ ]:


#performing labelEncoding on y i.e RainTommorrow
le = LabelEncoder()
df['RainTomorrow']= le.fit_transform(df['RainTomorrow'])
df.RainTomorrow.value_counts()


# In[ ]:


#Performing one hot encoding on RainToday, WindGustDir, WindDir9am, WindDir3pm, Location.
df_WindGustDir = pd.get_dummies(df["WindGustDir"], prefix="1")
df_WindDir9am = pd.get_dummies(df["WindDir9am"], prefix="2")
df_WindDir3pm = pd.get_dummies(df["WindDir3pm"], prefix="3")
df_RainToday = pd.get_dummies(df["RainToday"])
location_ohe = pd.get_dummies(df["Location"])
df_new = pd.concat([df, df_WindGustDir, df_WindDir9am, df_WindDir3pm, df_RainToday, location_ohe], axis=1)
df_new.drop(columns=["WindGustDir", "WindDir9am", "WindDir3pm", "RainToday", "Location"], axis=1, inplace=True)


# In[ ]:



#Taking average of columns annd create new column
df_new["AveTemp"] = (df_new["MinTemp"]+df_new["MaxTemp"])/2
df_new["WindSpeed12pm"] = (df_new["WindSpeed3pm"]+df_new["WindSpeed9am"])/2
df_new["Humidity12pm"] = (df_new["Humidity3pm"]+df_new["Humidity9am"])/2
df_new["Pressure12pm"] = (df_new["Pressure3pm"]+df_new["Pressure9am"])/2
df_new["Cloud12pm"] = (df_new["Cloud3pm"]+df_new["Cloud9am"])/2
df_new["Temp12am"] = (df_new["Temp3pm"]+df_new["Temp9am"])/2


# # Splitting test and train data

# In[ ]:


X = df_new.loc[:,df_new.columns != "RainTomorrow"]
y = df_new.loc[:,["RainTomorrow"]]


# In[ ]:


y.RainTomorrow.value_counts()


# In[ ]:


Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, train_size=0.80, random_state=2, )
print(Xtrain.shape)
print(Xtest.shape)
print(ytrain.shape)
print(ytest.shape)


# In[ ]:


# from imblearn.under_sampling import RandomUnderSampler
# from imblearn.over_sampling import RandomOverSampler
# rus = RandomOverSampler(random_state = 3)
# Xtrain, ytrain = rus.fit_sample(Xtrain, ytrain)


# In[ ]:


# from imblearn.over_sampling import SMOTE
# smote = SMOTE(ratio='minority')
# Xtrain, ytrain = smote.fit_sample(Xtrain, ytrain)
# Ensure columns are homogeneous - imblearn recasts as numpy array
# X_train_f_sm = pd.DataFrame(X_train_f_sm, columns=X_train_f.columns)


# # Performing standization

# In[ ]:


scaler = StandardScaler()
#fitting standardization on train data only
scaler.fit(Xtrain)
XtrainSTD = scaler.transform(Xtrain)
XtestSTD = scaler.transform(Xtest)


# In[ ]:


XtrainSTD.shape


# # Performing PCA

# In[ ]:


pca = PCA(n_components=50)
pca.fit(XtrainSTD)
XtrainPCA = pca.transform(XtrainSTD)
XtestPCA = pca.transform(XtestSTD)
XtrainPCA.shape


# In[ ]:


pca.explained_variance_ratio_


# In[ ]:


PC = ['PC1','PC2','PC3','PC4','PC5','PC6','PC7','PC8','PC9','PC10',
     'PC11','PC12','PC13','PC14','PC15','PC16','PC17','PC18','PC19','PC20',
     'PC21','PC22','PC23','PC24','PC25','PC26','PC27','PC28','PC29','PC30',
     'PC31','PC32','PC33','PC34','PC35','PC36','PC37','PC38','PC39','PC40',
     'PC41','PC42','PC43','PC44','PC45','PC46','PC47','PC48','PC49','PC50']
pca_df = pd.DataFrame({'var':pca.explained_variance_ratio_, 'PC':PC})
plt.figure(figsize=(30,10))
sns.barplot(x='PC',y="var", data=pca_df, color="c");


# # Modelling

# ### Random forest

# In[ ]:


rf = RandomForestClassifier(random_state=0)
parameters = {'bootstrap': [True, False],
              'min_samples_split':[2, 3, 4],
              'criterion':['entropy', 'gini'],
              'n_estimators':[100, 200]
             }
grid_search1 = GridSearchCV(estimator=rf, param_grid=parameters, refit='roc_auc', scoring=['accuracy', 'roc_auc'], cv=10, n_jobs=-1)
grid_search1 = grid_search1.fit(XtrainSTD, ytrain.values.ravel())


# In[ ]:


print("Best Parameters : ",grid_search1.best_params_)
print("Best AUC-ROC : ", grid_search1.best_score_)


# In[ ]:


#Building ranadom forest with these parameters
rf = RandomForestClassifier(bootstrap= False, criterion= 'entropy', min_samples_split= 4, n_estimators= 200, random_state=0)
rf.fit(XtrainSTD, ytrain.values.ravel())
ypred = rf.predict(XtestSTD)
accuracy = accuracy_score(ypred, ytest)
print(accuracy)


# In[ ]:


def plot_curve(model, X_test, y_test,score, model_label):
    
    # function to plot roc curve for the given model
    y_score = pd.DataFrame(model.predict_proba(X_test))[1]
    fpr,tpr, threshold = roc_curve(y_test, y_score)
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange', label='{} {}'.format(model_label,np.round(score,2)))
    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc="lower right")
    plt.show()
    plt.savefig('roc random forest.png')
    
plot_curve(rf, XtestSTD, ytest, grid_search1.best_score_, "Random Forest")


# In[ ]:


cnf_matrix = confusion_matrix(ypred, ytest)
sns.heatmap(cnf_matrix,cmap="coolwarm_r",annot=True,linewidths=0.5)
plt.title("Confusion_matrix")
plt.xlabel("Predicted_class")
plt.ylabel("Real class")
plt.show()


# ### Logistic regression

# In[ ]:


lr = LogisticRegression(random_state=0)
parameters = {'penalty': ['l1', 'l2'],
              'fit_intercept':[True, False]
             }
grid_search2 = GridSearchCV(estimator=lr, param_grid=parameters, refit='roc_auc', scoring=['accuracy', 'roc_auc'], cv=10, n_jobs=-1)
grid_search2 = grid_search2.fit(XtrainSTD, ytrain.values.ravel())


# In[ ]:


print("Best Parameters : ",grid_search2.best_params_)
print("Best AUC-ROC : ", grid_search2.best_score_)


# In[ ]:


lr = LogisticRegression(fit_intercept=True, penalty='l2', random_state=0)
lr.fit(XtrainSTD, ytrain)
predict = lr.predict(XtestSTD)
accuracy = accuracy_score(predict, ytest)
roc_score = roc_auc_score(predict, ytest)
print(accuracy)


# In[ ]:


def plot_curve(model, X_test, y_test,score, model_label):
    
    # function to plot roc curve for the given model
    y_score = pd.DataFrame(model.predict_proba(X_test))[1]
    fpr,tpr, threshold = roc_curve(y_test, y_score)
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange', label='{} {}'.format(model_label,np.round(score,2)))
    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc="lower right")
    plt.show()
    plt.savefig('roc2 logistic regression.png')
    
plot_curve(lr, XtestSTD, ytest, grid_search.best_score_, "Logistic Regression")


# ### Neural network

# In[ ]:


from sklearn.model_selection import cross_val_score


# In[ ]:


classifier = Sequential()
classifier.add(Dense(units=62, init='uniform', activation='relu', input_dim=124))
classifier.add(Dense(units=62, init='uniform', activation='relu'))
classifier.add(Dense(units=1, init='uniform', activation='sigmoid'))
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
classifier.fit(XtrainSTD, ytrain, batch_size=10, epochs=100)


# In[ ]:


ypred = classifier.predict(XtestSTD)
ypred = (ypred>0.5)
accuracy_NN = accuracy_score(ypred, ytest)
roc_NN = roc_auc_score(ypred, ytest)
accuracy_NN


# In[ ]:


roc_NN


# In[ ]:


def plot_curve(model, X_test, y_test,score, model_label):
    
    # function to plot roc curve for the given model
    y_score = pd.DataFrame(model.predict_proba(X_test))
    fpr,tpr, threshold = roc_curve(y_test, y_score)
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange', label='{} {}'.format(model_label,np.round(score,2)))
    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc="lower right")
    plt.show()
    plt.savefig('roc random forest.png')
    
plot_curve(classifier, XtestSTD, ytest, roc_NN, "Random Forest")

